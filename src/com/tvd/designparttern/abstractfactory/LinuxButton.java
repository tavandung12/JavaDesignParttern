package com.tvd.designparttern.abstractfactory;

public class LinuxButton extends Button {

	public LinuxButton() {
		setColor("color-linux");
	}
	
}
