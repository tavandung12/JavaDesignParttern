package com.tvd.designparttern.abstractfactory;

public class LinuxEditBox extends EditBox {

	public LinuxEditBox() {
		setStyle("style-linux");
	}
	
}
