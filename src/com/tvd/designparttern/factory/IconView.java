package com.tvd.designparttern.factory;

public interface IconView {

	public String getName();
	
	public void setName();
	
	public String getInfo();
	
}
