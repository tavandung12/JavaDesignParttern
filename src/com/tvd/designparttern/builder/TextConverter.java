package com.tvd.designparttern.builder;

public abstract class TextConverter {

	public abstract void convertCharacter(char c);
	
	public abstract void convertParagraph();
	
}
