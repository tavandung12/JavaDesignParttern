package com.tvd.designparttern.abstractfactory;

public abstract class LookAndFeel {

	public abstract EditBox createEditBox();
	
	public abstract Button createButton();
	
}
